<?php

use ttlt\fly\process\ProcessDescStruct;
use ttlt\fly\process\ServiceProcessManager;

include __DIR__ . '/../src/process/ServiceProcessManager.php';
include __DIR__ . '/../src/process/ProcessDescStruct.php';

//
/**
 * 带守护进程启动命令: php exampleProcessManager.php daemon
 * 不带守护进程启动命令: php exampleProcessManager.php
 * 不带守护进程启动命令: php exampleProcessManager.php start
 * 重启服务进程命令：php exampleProcessManager.php restart
 * 停止命令：php exampleProcessManager.php stop
 */
//简单使用
ServiceProcessManager::setup()->setDebug(true)->dispatch();

/*
//高级使用
ServiceProcessManager::setup([
    ProcessDescStruct::create('', 'actionOne', 1),
    ProcessDescStruct::create('', 'actionTwo', 1)
])
->setDebug(true)
->setOsUser('www')
->setDaemonProcessName('ProcessName')
->setDaemonLoopInterval(1000)
->dispatch();
*/

/*
//灵活使用
ServiceProcessManager::setup(ProcessDescStruct::create('','actionOne',1));
ServiceProcessManager::setup(ProcessDescStruct::create('','actionTwo',1));
$manager = ServiceProcessManager::getInstance();
$manager->setDebug(true);
$manager->dispatch();
*/

//下面是业务逻辑例子
while (1) {
    //获取进程pid
    if (function_exists('posix_getpid')) {
        $pid = posix_getpid();
    } else {
        $pid = getmypid();
    }
    //接收父进程命令
    ServiceProcessManager::getInstance()->recvProcessCmd();
    global $argv;
    echo "[childProcess] [pid=" . $pid . '] [command=php ' . implode(' ',$argv) .']'. PHP_EOL;
    sleep(1);
}