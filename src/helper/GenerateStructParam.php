<?php
namespace ttlt\fly\helper;

use think\Db;
use think\facade\Config;

/**
 * class GenerateStructParam
 * 生成thinkphp通用参数类，并加上注释；规则是用数据库的字段作为参数名称
 */
class GenerateStructParam
{
    protected $config;
    public function __construct($config = [])
    {
        if (empty($config['database'])) {
            $config['database'] = Config::get('database.database');
        }
        if (empty($config['prefix'])) {
            $config['prefix'] = Config::get('database.prefix');
        }
        if (substr($config['prefix'], -1, 1) == '_') {
            $config['prefix'] = substr($config['prefix'], 0, strlen($config['prefix']) - 1);
        }
        if (empty($config['struct_dir'])) {
            $config['struct_dir'] = realpath(__DIR__ . '/../../../../../') . '/application/common/struct';
        }
        if (!is_dir($config['struct_dir'])) {
            mkdir($config['struct_dir'], 0755, true);
        }
        if (empty($config['namespace'])) {
            $config['namespace'] = 'app\common\struct';
        }
        echo '<pre>配置如下：' . PHP_EOL;
        var_export($config);
        echo PHP_EOL;
        $this->config = $config;
    }
    public function run()
    {
        $sql = "SELECT table_name,table_comment FROM information_schema.`TABLES` WHERE TABLE_SCHEMA = '" . $this->config['database'] . "';";
        $tables = Db::query($sql);
        $columns = [];
        foreach ($tables as $table) {
            if (!empty($this->config['table_name']) && $table['table_name'] != $this->config['table_name']) {
                continue;
            }
            $sql = "select column_name,data_type,column_key,column_type,column_comment from information_schema.`COLUMNS`
                        where TABLE_SCHEMA='" . $this->config['database'] . "' and table_name='" . $table['table_name'] . "' order by ORDINAL_POSITION";
            $tmp = db::query($sql);
            $columns = array_merge($columns,$tmp);
        }
        $this->runOnce($columns);

        echo '</pre>';
    }
    protected function runOnce($columns)
    {
        $className = "Param";
        //获取代码内容
        $file = $this->config['struct_dir'] . '/' . $className . '.php';
        $content = $this->getContent($className);

        //备份代码
        if (is_file($file)) {
            $bakDir = $this->config['model_dir'] . '/bak/' . date("YmdHis");
            if (!is_dir($bakDir)) {
                mkdir($bakDir, 0755, true);
            }
            $bakFile = $bakDir . '/' . $className . '.php';
            file_put_contents($bakFile, file_get_contents($file));
        }

        //去除以前的注释
        $reg = "/\s*\/\*\*.*\/\s*class\s+${className}\s+extends/is";
        $content = preg_replace($reg, PHP_EOL . PHP_EOL . "class {$className} extends", $content, -1);

        //组合成新的内容
        $newLines = [];
        $lines = explode(PHP_EOL, $content);
        $notes = $this->generateNotes($columns);
        $regClass = "/^\s*class\s+${className}\s+extends/i";
        foreach ($lines as $k => $line) {
            //最后一行退出
            if (count($lines) - 1 == $k) {
                $newLines[] = $line;
                break;
            }
            //加入注释
            if (preg_match($regClass, $lines[$k + 1])) {
                $newLines[] = $line;
                $newLines = array_merge($newLines, $notes);
                continue;
            }
            $newLines[] = $line;
        }
        $content = implode(PHP_EOL, $newLines);
        //echo $content;exit;
        //echo $file . PHP_EOL;exit;
        file_put_contents($file, $content);
        echo '保存文件：' . realpath($file) . PHP_EOL;
    }
    protected function getContent( $className)
    {
        $str = <<<EOF
<?php
namespace {$this->config['namespace']};

class {$className} extends \ArrayObject
{
    public static function init(\$array){
        return new self(\$array);
    }
    public function __get(\$key){
        if( !isset(\$this[\$key]) ){
            return null;
        }
        return \$this[\$key];
    }
    public function __set(\$key,\$value){
        \$this[\$key] = \$value;
    }
    public function __unset(\$key){
        unset(\$this[\$key]);
    }
    public function __isset(\$key){
        return isset(\$this[\$key]);
    }
    public function createNew(\$keyStr){
        \$keyStr = preg_replace('/\s/','',\$keyStr);
        \$keys = explode(',', \$keyStr);
        \$data = [];
        foreach (\$keys as \$key) {
            if( strstr(\$key,'->') == false ){
                \$data[\$key] = \$this->\$key;
                continue;
            }
            \$tmp = explode('->',\$key);
            if( isset(\$this[\$tmp[0]]) ){
                \$data[\$tmp[1]] = \$this[\$tmp[0]];
                unset(\$this[\$tmp[0]]);
            }else{
                \$data[\$tmp[1]] = null;
            }
        }
        return self::init(\$data);
    }
}
EOF;
        return $str;
    }
    protected function generateNotes($columns)
    {
        $notes = [
            "/**",
            " * 通用参数结构类，用于提示",
            " * class Param ",
            " * @package app\common\struct",
        ];

        $types = [
            'tinyint' => 'integer',
            'smallint' => 'integer',
            'mediumint' => 'integer',
            'int' => 'integer',
            'bigint' => 'integer',

            'float' => 'float',
            'double' => 'float',
            'decimal' => 'float',

            'timestamp' => 'string',
            'time' => 'string',
            'date' => 'string',
            'datetime' => 'string',

            'set' => 'string',
            'enum' => 'string',
            'blob' => 'string',
            'text' => 'string',
            'varchar' => 'string',
            'char' => 'string',
            'mixed' => 'mixed'
        ];
        foreach ($columns as $k=>$column) {
            $column['data_type'] = strtolower($column['data_type']);
            if (isset($types[$column['data_type']])) {
                $columns[$k]['data_type'] = $types[$column['data_type']];
            } else {
                echo 'WARNING：' . $column['data_type'] . ' 类型没有找到' . PHP_EOL;
                $type = 'string';
            }
        }
        //var_export($columns);exit;
        do{
            $column = array_shift($columns);
            $data_type = $column['data_type'];
            $column_comment = $column['column_comment'];
            for($i=0;$i<count($columns);$i++){
                if( !isset($columns[$i]) ){
                    continue;
                }
                if( $column['column_name'] == $columns[$i]['column_name'] ){
                    if( $data_type != $columns[$i]['column_name'] ){
                        $data_type = "mixed";
                    }
                    $column_comment = "";
                    unset($columns[$i]);
                }
            }
            $notes[] = ' * @property ' . $data_type . ' ' . $column['column_name'] . ' ' . $column_comment;
            $columns = array_values($columns);
        }while(count($columns) > 0);
        $notes[] = ' */';
        $notes[] = '';
        return $notes;
    }
}
