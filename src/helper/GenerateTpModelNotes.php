<?php
namespace ttlt\fly\helper;

use think\Db;
use think\facade\Config;

/**
 * class GenerateTpModelNotes
 * 生成thinkphp model类，并加上注释
 */
class GenerateTpModelNotes
{
    protected $config;
    public function __construct($config = [])
    {
        if (empty($config['database'])) {
            $config['database'] = Config::get('database.database');
        }
        if (empty($config['prefix'])) {
            $config['prefix'] = Config::get('database.prefix');
        }
        if (substr($config['prefix'], -1, 1) == '_') {
            $config['prefix'] = substr($config['prefix'], 0, strlen($config['prefix']) - 1);
        }
        if (empty($config['model_dir'])) {
            $config['model_dir'] = realpath(__DIR__ . '/../../../../../') . '/application/common/model';
        }
        if (!is_dir($config['model_dir'])) {
            mkdir($config['model_dir'], 0755, true);
        }
        if (empty($config['namespace'])) {
            $config['namespace'] = 'app\common\model';
        }
        echo '<pre>配置如下：' . PHP_EOL;
        var_export($config);
        echo PHP_EOL;
        $this->config = $config;
    }
    public function run()
    {
        $sql = "SELECT table_name,table_comment FROM information_schema.`TABLES` WHERE TABLE_SCHEMA = '" . $this->config['database'] . "';";
        $tables = Db::query($sql);
        foreach ($tables as $table) {
            if (!empty($this->config['table_name']) && $table['table_name'] != $this->config['table_name']) {
                continue;
            }
            $sql = "select column_name,data_type,column_key,column_type,column_comment from information_schema.`COLUMNS`
                        where TABLE_SCHEMA='" . $this->config['database'] . "' and table_name='" . $table['table_name'] . "' order by ORDINAL_POSITION";
            $columns = db::query($sql);
            $this->runOnce($table, $columns);
        }
        echo '</pre>';
    }
    protected function runOnce($table, $columns)
    {
        //生成类名称
        $arr = explode('_', $table['table_name']);
        if ($arr[0] == $this->config['prefix'] && count($arr) > 0) {
            $prefix = array_shift($arr);
        } else {
            $prefix = '';
        }
        if (preg_match('/^[a-zA-Z]/', $arr[0])) {
            $arr[0] = ucfirst($arr[0]);
        } else {
            array_unshift($arr, 'Model');
        }
        $className = implode(' ', $arr);
        $className = ucwords($className);
        $className = str_replace(' ', '', $className);

        //获取代码内容
        $file = $this->config['model_dir'] . '/' . $className . '.php';
        $content = $this->getContent($file, $className);

        //备份代码
        if (is_file($file)) {
            $bakDir = $this->config['model_dir'] . '/bak/' . date("YmdHis");
            if (!is_dir($bakDir)) {
                mkdir($bakDir, 0755, true);
            }
            $bakFile = $bakDir . '/' . $className . '.php';
            file_put_contents($bakFile, file_get_contents($file));
        }

        //去除以前的注释
        $reg = "/\s*\/\*\*.*\/\s*class\s+${className}\s+extends/is";
        $content = preg_replace($reg, PHP_EOL . PHP_EOL . "class {$className} extends", $content, -1);

        //组合成新的内容
        $newLines = [];
        $lines = explode(PHP_EOL, $content);
        $notes = $this->generateNotes($className, $columns, $table);
        $regClass = "/^\s*class\s+${className}\s+extends.+\{/i";
        foreach ($lines as $k => $line) {
            //最后一行退出
            if (count($lines) - 1 == $k) {
                $newLines[] = $line;
                break;
            }
            //加入注释
            if (preg_match($regClass, $lines[$k + 1])) {
                $newLines[] = $line;
                $newLines = array_merge($newLines, $notes);
                continue;
            }
            $newLines[] = $line;
        }

        //前缀不一致的，加入表名；
        $reg = '/protected\s+\$table\s*\=\s*/i';
        if ($prefix != $this->config['prefix'] && !preg_match($reg, $content)) {
            $lines = $newLines;
            $newLines = [];
            foreach ($lines as $k => $line) {
                $newLines[] = $line;
                if (preg_match($regClass, $lines[$k])) {
                    $newLines[] = PHP_EOL . '   protected $table="' . $table['table_name'] . '";';
                }
            }
        }

        //加入$pk
        $reg = '/protected\s+\$pk\s*\=\s*/i';
        if (!preg_match($reg, $content)) {
            $pkColumn = '';
            foreach ($columns as $column) {
                if ('PRI' == $column['column_key']) {
                    $pkColumn = $column['column_name'];
                }
            }
            if ('' != $pkColumn) {
                $lines = $newLines;
                $newLines = [];
                foreach ($lines as $k => $line) {
                    $newLines[] = $line;
                    if (preg_match($regClass, $lines[$k])) {
                        $newLines[] = PHP_EOL . '   protected $pk="' . $pkColumn . '";';
                    }
                }
            }
        }
        $content = implode(PHP_EOL, $newLines);
        //echo $content;exit;
        //echo $file . PHP_EOL;exit;
        file_put_contents($file, $content);
        echo '保存文件：' . realpath($file) . PHP_EOL;
    }
    protected function getContent($file, $className)
    {
        $str = <<<EOF
<?php
namespace {$this->config['namespace']};
use think\Model;

class {$className} extends Model{

}
EOF;

        if( strtolower($className) == "model" ){
            $str = <<<EOF
<?php
namespace {$this->config['namespace']};
use think\Model as thinkModel;

class {$className} extends thinkModel{

}
EOF;
        }
        if (is_file($file)) {
            $content = file_get_contents($file);
            if (trim($content) == '') {
                $content = $str;
            }
        } else {
            $content = $str;
        }
        return $content;
    }
    protected function generateNotes($className, $columns, $table)
    {
        $notes = [
            "/**",
            " * {$table['table_comment']}",
            " * class {$className} ",
            " * @package app\common\model",
        ];

        $types = [
            'tinyint' => 'integer',
            'smallint' => 'integer',
            'mediumint' => 'integer',
            'int' => 'integer',
            'bigint' => 'integer',

            'float' => 'float',
            'double' => 'float',
            'decimal' => 'float',

            'timestamp' => 'string',
            'time' => 'string',
            'date' => 'string',
            'datetime' => 'string',

            'set' => 'string',
            'enum' => 'string',
            'blob' => 'string',
            'text' => 'string',
            'varchar' => 'string',
            'char' => 'string',
        ];
        foreach ($columns as $column) {
            $column['data_type'] = strtolower($column['data_type']);
            if (isset($types[$column['data_type']])) {
                $type = $types[$column['data_type']];
            } else {
                echo 'WARNING：' . $column['data_type'] . ' 类型没有找到' . PHP_EOL;
                $type = 'string';
            }
            $notes[] = ' * @property ' . $type . ' ' . $column['column_name'] . ' ' . $column['column_comment'];
        }
        foreach ($columns as $column) {
            $column['data_type'] = strtolower($column['data_type']);
            if (isset($types[$column['data_type']])) {
                $type = $types[$column['data_type']];
            } else {
                echo 'WARNING：' . $column['data_type'] . ' 类型没有找到' . PHP_EOL;
                $type = 'string';
            }
            $arr = explode('_', $column['column_name']);
            array_unshift($arr, 'by');
            array_unshift($arr, 'get');
            $functionName = implode(' ', $arr);
            $functionName = ucwords($functionName);
            $functionName = str_replace(' ', '', $functionName);
            $functionName = lcfirst($functionName);
            $notes[] = ' * @method ' . "\\" . $this->config['namespace'] . '\\' . $className . ' ' . $functionName . "(${type} \$${column['column_name']})" . ' static ';
        }
        $notes[] = ' */';
        $notes[] = '';
        return $notes;
    }
}
