<?php
namespace ttlt\fly\helper;

/**
 * class TreeRecord
 * 二维数组转树形结构
 */
class TreeRecord{
    //@var array $records 二维数组
    protected $records;
    //@var string $idColumnName id字段名称
    protected $idColumnName;
    //@var string $pidColumnName 父id字段名称
    protected $pidColumnName;
    /**
     * __construct()
     *
     * @param array $records 二维数组
     * @param string $idColumnName id字段名称
     * @param string $pidColumnName 父id字段名称
     */
    public function __construct($records,$idColumnName="id",$pidColumnName="pid")
    {
        $this->records = $records;
        $this->idColumnName = $idColumnName;
        $this->pidColumnName = $pidColumnName;
    }
    /**
     * 查找下级记录
     *
     * @param integer|string $id id值
     * @return array
     */
    public function findChildRcords($id){
        $records = [];
        foreach( $this->records as $record ){
            if( $record[$this->pidColumnName] == $id ){
                $records[] = $record;
                $records = array_merge($records,$this->findChildRcords($record[$this->idColumnName]));
            }
        }
        return $records;
    }
    /**
     * 查找父记录
     *
     * @param integer|string $id id值
     * @param array $curr 当前记录，默认为空；记录里面没有当前的记录数据，需要传该参数
     * @return array
     */
    public function findParentRecords($id,$curr = null){
        $records = [];
        if( $curr == null ){
            foreach( $this->records as $record ){
                if( $record[$this->idColumnName] == $id ){
                    $curr = $record;
                    break;
                }
            }
            if( $curr == null ){
                return [];
            }
        }
        $pid = $curr[$this->pidColumnName];
        foreach( $this->records as $record ){
            if( $record[$this->idColumnName] == $pid ){
                $records[] = $record;
                $records = array_merge($records,$this->findParentRecords($id,$record));
            }
        }
        return $records;
    }
    /**
     * 转成树形结构
     *
     * @param integer|string $topId 父id数值
     * @param string $childrenColumnName 存子字段数据名称
     * @return array
     */
    public function formatToTree($topId = 0,$childrenColumnName='children'){
        $records = [];
        foreach( $this->records as $record ){
            if( $record[$this->pidColumnName] == $topId ){
                $record[$childrenColumnName] = $this->formatToTree($record[$this->idColumnName],$childrenColumnName); 
                $records[] = $record;
            }
        }
        return $records;
    }
    /**
     * 修复上下级断层
     *
     * @param integer|string $topId 顶级id值
     * @return self
     */
    public function repair($topId = 0){
        $res = [];
        foreach( $this->records as $record ){
            $res[$record[$this->idColumnName]] = $record;
        }
        foreach( $this->records as $k=>$record ){
            $pid = $record[$this->pidColumnName];
            if( $pid != $topId && !isset($res[$pid]) ){
                $this->records[$k][$this->pidColumnName] = $topId;
            }
        }
        return $this;
    }
}