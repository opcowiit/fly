<?php

namespace ttlt\fly\process;

/**
 * class ProcessDescStruct 
 * 进程描述的数据结构
 */
class ProcessDescStruct extends \ArrayObject
{
    //@var string $action 子进程方法
    public $action = '';

    //@var integer $count 启动子进程数量
    public $count = 1;

    //@var 命令选项
    public $command = '';

    //@var array $descriptorspec pipe描述符
    public $descriptorspec = [];

    //@var array $pipes 输入，输出，错误的管道
    public $pipes = [];

    //@var string $cwd 运行的目录
    public $cwd = null;

    //@var array $env_vars 环境变量
    public $env_vars = null;

    //@var array $options 选项
    public $options = null;

    //@var array $procs 进程资源
    public $procs = [];

    static function init($arr){
        $self = new self;
        foreach( $arr as $k=>$v ){
            if( isset($self->$k) ){
                $self->$k = $v;
            }
        }
        return $self;
    }

    static function create($command = '',$action = '',$count=1,$descriptorspec=[],$cwd=null,$env_vars=null,$options=null){
        $self = new self;

        if( $command != ''){
            $self->command = $command;
        }else{
            global $argv;
            $arr = array_slice($argv, 0, count($argv) - 1);
            $self->command = 'php ' . implode(' ', $arr);
            if( $action !== '' ){
                $self->command .= ' ' . $action;
            }
        }
        $self->action = $action;
        $self->count = $count;
        $self->descriptorspec = $descriptorspec;
        $self->cwd = $cwd;
        $self->env_vars = $env_vars;
        $self->options = $options;
        return $self;
    }
}